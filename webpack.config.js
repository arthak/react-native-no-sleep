const path = require('path');

module.exports = env => ({
  entry: './src/index.js',
  devtool: env === 'dev' ? 'inline-source-map' : undefined,
  externals: ['react', 'react-native'],
  mode: env === 'dev' ? 'development' : 'production',
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react'],
            plugins: [
              [
                '@babel/plugin-transform-runtime',
                {
                  regenerator: true,
                },
              ],
            ],
          },
        },
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: ['node_modules'],
  },
  output: {
    path: path.resolve('./dist'),
    filename: 'index.js',
    libraryTarget: 'umd',
  },
});
