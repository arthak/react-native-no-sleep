#import "NoSleep.h"

@implementation NoSleep

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(preventSleep)
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    });
}

RCT_EXPORT_METHOD(allowSleep)
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    });
}

@end
