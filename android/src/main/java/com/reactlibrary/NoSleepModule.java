package com.reactlibrary;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import android.app.Activity;
import android.view.WindowManager;

public class NoSleepModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;

    public NoSleepModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "NoSleep";
    }

    private void setScreenOnFlag(boolean on) {
        final Activity currentActivity = getCurrentActivity();

        if (currentActivity == null) {
            return;
        }
        class Setter implements Runnable {
            boolean on;
            Activity activity;
            Setter(boolean on, Activity activity) {
                this.on = on;
                this.activity = activity;
            }

            @Override
            public void run() {
                if (on) {
                    activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                } else {
                    activity.getWindow().clearFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                }
            }
        }
        currentActivity.runOnUiThread(new Setter(on, currentActivity));
    }

    @ReactMethod
    public void preventSleep() {
        this.setScreenOnFlag(true);
    }

    @ReactMethod
    public void allowSleep() {
        this.setScreenOnFlag(false);
    }
}
