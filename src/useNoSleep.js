import { useEffect } from 'react';
import NoSleepNative from './native';

let numConsumers = 0;

export default () => {
  useEffect(() => {
    const wasSleepingAllowed = numConsumers === 0;
    numConsumers += 1;
    if (wasSleepingAllowed) {
      NoSleepNative.preventSleep();
    }

    return () => {
      numConsumers -= 1;
      if (numConsumers === 0) {
        NoSleepNative.allowSleep();
      }
    };
  }, []);
};
