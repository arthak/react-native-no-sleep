import { NativeModules } from 'react-native';

const { NoSleep } = NativeModules;

export default NoSleep;
