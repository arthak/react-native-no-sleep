import useNoSleep from './useNoSleep';

export default () => {
  useNoSleep();
  return null;
};
