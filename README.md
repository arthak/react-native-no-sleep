Prevents devices from going to sleep due to inactivity. Useful for apps in which users watch the app without interaction, e.g. video or audio apps, games, exercise tools etc.

# react-native-no-sleep
The library targets pure React Native projects, with post-0.60 React Native and post-16.8 React with hook support. For expo-based projects, see `expo-keep-awake`.

## Getting started
`$ npm install --save react-native-no-sleep`

### Autolink
...

## Usage
### Example 1 - Hook based
```javascript
import { useNoSleep } from 'react-native-no-sleep';

const GameScreen = () => {
    useNoSleep();

    return (
	  <View>
		<Text>
		  I don't sleep while mounted
		</Text>
	  </View>
    );
};
```

### Example 2 - Render based
```javascript
import { NoSleep } from 'react-native-no-sleep';

const GameScreen = () => (
    return (
	  <View>
	    <NoSleep />
		<Text>
		  I don't sleep while mounted
		</Text>
	  </View>
    );
};
```

